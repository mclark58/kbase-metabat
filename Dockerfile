FROM kbase/sdkbase2:latest
MAINTAINER KBase Developer
# -----------------------------------------
# In this section, you can install any system dependencies required
# to run your App.  For instance, you could place an apt-get update or
# install line here, a git checkout to download code, or run any other
# installation scripts.

RUN pip install pandas

WORKDIR /kb/module

ENV PATH=/kb/deployment/bin:$PATH

COPY metabat_binary /kb/deployment/bin/metabat_binary/
ENV PATH=/kb/deployment/bin/metabat_binary:$PATH
ENV PATH=/kb/deployment/bin/metabat_binary/bbmap:$PATH
ENV PATH=/kb/deployment/bin/metabat_binary/samtools/bin:$PATH

COPY ./ /kb/module
RUN mkdir -p /kb/module/work
RUN chmod -R a+rw /kb/module

RUN make all

ENTRYPOINT [ "./scripts/entrypoint.sh" ]

CMD [ ]
